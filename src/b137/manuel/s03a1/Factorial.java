package b137.manuel.s03a1;

import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {
        System.out.println("Factorial\n");

        Scanner appScanner = new Scanner(System.in);
        System.out.println("Please input number to get its factorial: ");
        int number = appScanner.nextInt();
        long factorial = multiplyNumbers(number);
        System.out.println("Factorial of " + number + " = " + factorial);

        //create a java program that accepts an integer and computer for
        // the factorial value and displays it to the console.
    }
    public static long multiplyNumbers(int number) {
        if (number >= 1)
            return number * multiplyNumbers(number-1);
        else
            return 1;
    }
}
